# acortador-urls

La siguiente prueba consiste en construir los servicios web necesarios usando como referencia API REST FULL, los servicios web deben ser capaz de acortar URLS en general, podrías usar Node.js, Python o PHP para el desarrollo de esta prueba.

# Requerimientos

1. Laravel se requiere de un servidor apache o nginx que cumpla con las siguientes características:
    - PHP> = 7.1.3
    - Extensión PHP OpenSSL
    - Extensión PHP de DOP
    - Mbstring PHP Extension
    - Tokenizer PHP Extension
    - Extensión PHP PHP
    - Ctype PHP Extension
    - JSON PHP Extension

2. Para montar el entorno de desarrollo laravel se necesita instalar lo siguiente
    - Composer
    - Git
    - Node
    - NPM 

# Instalación del proyecto

1. Descargar/clonar el repositorio en su directorio de proyectos de su entorno de desarrollo laravel 

2. Entrar a la carpeta raiz del proyecto y ejecutar el comando "composer install" para descargar las depencias backend
    
3. Crear una base de datos, por defecto la configuración es para una bd mysql.

4. Establecer las variables del entorno en el archivo .env
    - En este archivo se encuentra toda la configuracion necesaria para arrancar el proyecto:
        - host
        - puerto
        - tipo de bd
        - base de datos
        - usuario de bd
        - contraseña de bd
        - url de compilacion de recursos
        
5. Una vez configurados las variables del entorno de desarrollo es posible correr el las migraciones para generar las tablas en la bd.
    - Abrir la consola en el directorio raiz del proyecto
    - Ejecutar el comando "php artisan migrate"
    - Listo, las tablas de la bd han sido creadas.

6. Descargar las dependencias node
    - Abrir la consola en el directorio raiz del proyecto
    - Ejecutar el comando "npm install"

7. Visualizar el proyecto llendo a la direccion del proyecto en el host local, por ejemplo: http//localhos:8000/prueba-backend/public.
    - Esta dirección muestra las interfaces requeridas en la prueba.

8. Visualizar el api en el cual se alijan los servicios llendo a la direccion del proyecto en el host local, por ejemplo: http://localhost:8000/prueba-backend/public/api
    - Esta dirección puede ser vista en *POSTMAN* para probar los diferentes servicios de la prueba
        - En el directorio raiz se encuentra el archivo "acortador_urls.postman_collection.json" que se puede importar para tener el entorno de pruebas del api disponible. Hay que configurar las varibles del entorno importado para que coincidan con el entorno local.
        
       
       
       
           
        
        