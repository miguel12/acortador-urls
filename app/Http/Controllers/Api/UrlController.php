<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StoreUrl;
use App\Http\Requests\StoreUrlCollection;
use function array_push;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Url;
use function response;

class UrlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Url::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUrl $request)
    {
        $url_nueva = $this->saveUrl($request->url_original);
        return $url_nueva->url_acortada;
    }

    /**
     * Store a newly created collection resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCollection(StoreUrlCollection $request)
    {
        $urls_acortadas = [];
        foreach ($request->all() as $url){
            $url_nueva = $this->saveUrl($url["url_original"]);
            array_push($urls_acortadas, $url_nueva->url_acortada);
        }

        return $urls_acortadas;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $user = Url::where('codigo', $codigo);
        if($user->exists()){
           $user_existente = $user->first();
           return redirect($user_existente->url_original);

        }else{
            return response()->json(["error" => "La url no exite"], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Genera un codigo pseudoaleatorio a partir de un numero de bytes
     * @return string
     */
    public function generateShortCode(){
        $cstrong = true;
        $bytes = openssl_random_pseudo_bytes(3, $cstrong);
        $short_link = bin2hex($bytes);
        return $short_link;
    }

    public function saveUrl($url_original){
        $codigo = $this->generateShortCode($url_original);
        $url = new Url;
        $url->url_original = $url_original;
        $url->url_acortada = url("api/".$codigo);
        $url->codigo = $codigo;
        $url->save();

        return $url;
    }
}
