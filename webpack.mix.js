const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

var assetsDir         = 'resources/assets/',
    nodeDir           = 'node_modules/',
    publicDir         = 'public/',
    distDir           = 'public/dist/';

mix.js('resources/js/app.js', 'public/js')
    .sourceMaps()
    .sass('resources/sass/app.scss', 'public/css')
    .options({ processCssUrls: false })
    .browserSync({
        proxy: process.env.MIX_BROWSERSYNC_PROXY
    });
